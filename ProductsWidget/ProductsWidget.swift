//
//  ProductsWidget.swift
//  ProductsWidget
//
//  Created by Anton on 02.09.20.
//

import WidgetKit
import SwiftUI


@main
struct ProductsWidget: Widget {
    let kind: String = "Products"
    
    var body: some WidgetConfiguration {
        IntentConfiguration(kind: kind, intent: FeedCategoryIntent.self, provider: WidgetTimelineProvider()) { entry in
            WidgetFeedView(entry: entry)
        }
//        StaticConfiguration(kind: kind, provider: WidgetTimelineProvider()) { entry in
//            WidgetFeedView(entry: entry)
//        }
        .configurationDisplayName("Discover & Shop")
        .description("Discover great products, daily")
        .supportedFamilies([.systemSmall, .systemMedium, .systemLarge])
    }
}
