//
//  JSONLoader.swift
//  GadgetFlowWidgets
//
//  Created by Anton on 27.09.20.
//

import Foundation

struct DecodedArray<T: Decodable & Equatable>: Decodable, Equatable {
    typealias DecodedTypeArray = [T]
    var array: DecodedTypeArray
    
    static func == (lhs: DecodedArray<T>, rhs: DecodedArray<T>) -> Bool {
        return lhs.array.elementsEqual(rhs.array)
    }
    
    private struct DynamicCodingKeys: CodingKey {
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }
        
        var intValue: Int?
        init?(intValue: Int) {
//            self.intValue = intValue
            return nil
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: DynamicCodingKeys.self)
        
        var tempArray = DecodedTypeArray()
        let keys = container.allKeys.sorted {$0.stringValue < $1.stringValue}
        for key in keys {
            do {
                let decodedObject = try container.decode(T.self, forKey: DynamicCodingKeys(stringValue: key.stringValue)!)
                tempArray.append(decodedObject)
            } catch {
                print("Unable to decode value for key \(key.stringValue)")
            }
        }
        dump(tempArray)
        array = tempArray
    }
}

class JSONLoader {
    func readFromBundle<T: Decodable>(file name:String, as type: T.Type = T.self) throws -> T {
        guard let bundlePath = Bundle(for: Self.self).path(forResource: name, ofType: "json") else { throw ParsingError.fileNotFound }
        
        let jsonData:Data
        do {
            jsonData = try Data(contentsOf: URL(fileURLWithPath: bundlePath))
        } catch {
            throw ParsingError.fileNotFound
        }
        return try decode(T.self, from: jsonData)
    }
    
    func readFromURL<T: Decodable>(_ url:URL, as type:T.Type = T.self) throws -> T {
        do {
            let jsonData = try String(contentsOf: url).data(using: .utf8)!
            return try decode(T.self, from: jsonData)
        } catch {
            throw ParsingError.loadError
        }
    }
    
    internal func decode<T>(_ type:T.Type, from json: Data) throws -> T where T: Decodable {
        do {
            return try JSONDecoder().decode(type, from: json)
        } catch {
            throw ParsingError.parseError
        }
        
    }
}


enum ParsingError:Error {
    case parseError
    case loadError
    case fileNotFound
}

extension ParsingError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .parseError:
            return "Unable to parse json"
        case .fileNotFound:
            return "Unable to find file in bundle"
        case .loadError:
            return "Unable to load file"
        }
        
    }
}
