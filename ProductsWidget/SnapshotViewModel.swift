//
//  SnapshotViewModel.swift
//  ProductsWidget
//
//  Created by Anton on 26.09.20.
//

import Foundation

struct SnapshotViewModel {
    internal static let snapshot_filename = "snapshot_items"
    lazy private var products:[ProductModel]? = {
        do {
            return try JSONLoader().readFromBundle(file: SnapshotViewModel.snapshot_filename, as: DecodedArray<ProductModel>.self).array
        } catch {
            print("SnapshotViewModel: Unable to prepare model")
        }
        return nil
    }()
    
    public func getProducts() -> [ProductModel]? {
        var mutableSelf = self
        return mutableSelf.products
    }
    
    public func getProducts(max:Int, random rand:Bool) -> [ProductModel] {
        var result:[ProductModel] = []
        if var products = rand ? self.getProducts()?.shuffled() : self.getProducts() {
            let count = products.count
            for _ in 0..<min(max, count) {
                result.append(products.removeFirst())
            }
        }
        return result
    }
    
    
}



