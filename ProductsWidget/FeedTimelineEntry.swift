//
//  FeedEntry.swift
//  ProductsWidget
//
//  Created by Anton on 27.08.20.
//

import WidgetKit
import Foundation
import GadgetFlowComponents

struct FeedTimelineEntry: TimelineEntry {
    let date: Date
    let items: [ProductModel]
    let feedCategory: WidgetFeedCategory
}
