//
//  WidgetUI.swift
//  ProductsWidget
//
//  Created by Anton on 27.08.20.
//

import WidgetKit
import SwiftUI
import GadgetFlowComponents


struct WidgetEntryView : View {
    var entry: WidgetTimelineProvider.Entry

    var body: some View {
        VStack(alignment: .leading, spacing: 2, content: {
            Text((entry.items.first?.imageURL)!)
            Text((entry.items.first?.imageURL)!)
        })
    }
}

struct PlaceholderWidgetEntryView : View {
    var body: some View {
        Text("Loading details. Please wait...")
    }
}

struct FeedCellView : View {
    @State var item:ProductModel
    var body: some View {
        Link(destination:productDeepLink(productId: String(item.productId))) {
            GeometryReader { geometry in
                let titleHeight:CGFloat = 35.0
                let sideSize = geometry.size.height - titleHeight
                VStack(alignment: .leading, spacing: 0, content: {
                    ImageViewContainer(imageURL: item.imageURL)
                        .frame(width: sideSize, height: sideSize, alignment: .center)
                        .cornerRadius(10)
//                        .clipShape(ContainerRelativeShape())
                        
                    Text(item.title)
                        .font(.system(size: 10))
                        .fontWeight(.medium)
                        .foregroundColor(.white)
                        .frame(height:titleHeight)
                        .lineLimit(2)
                        .frame(maxWidth: .infinity)
//                        .border(Color.blue)
                })
//                .background(Color.red)
                .frame(width: sideSize, alignment: .center)
            }
        }
    }
}

struct WidgetHeader: View {
    let title:String
    var body: some View {
        Group {
            HStack(alignment: .center, spacing: 0, content: {
                Text(title)
                    .foregroundColor(Color.init(rgb: 0xeebfc6))
                    .font(.system(size: 13))
                    .fontWeight(.heavy)
                Spacer()
                Image("iconWidget")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(height: 25)
            })
            .frame(height: 40)
            .padding(EdgeInsets.init(top: 0, leading: 15, bottom: 0, trailing: 10))
            .background(Color.init(rgb: 0xca374e))
            Rectangle()
                .fill(Color.init(rgb: 0xd15367))
                .frame(height: 1)
        }
    }
}

struct FeedCellsRow: View {
    let items: [ProductModel]
    
    var body: some View {
        HStack(alignment: .center, spacing: 0, content: {
            ForEach(0 ..< items.count) { index in
                let item = items[index]
                Link(destination:productDeepLink(productId: String(item.productId))) {
                    FeedCellView(item: item)
                }
                .frame(maxWidth:.infinity)
            }
        })
//        .background(Color.yellow)
//        .frame(alignment:.trailing)
    }
}

struct WidgetFeedView : View {
    var entry: WidgetTimelineProvider.Entry
    @Environment(\.widgetFamily) private var widgetFamily
    
    var body: some View {
        if (entry.items.count == 0) {
            Text("Data unavailable.\nPlease try again later.")
        } else {
            switch widgetFamily {
            case .systemSmall: SmallFeedView(entry: entry)
            case .systemMedium: MediumFeedView(entry: entry)
            case .systemLarge: LargeFeedView(entry: entry)
            default: SmallFeedView(entry: entry)
//            default: LargeFeedView(entry: entry)
            }
        }
    }
}

struct SmallFeedView : View {
    var entry: WidgetTimelineProvider.Entry
    
    var body: some View {
        let hPadding:CGFloat = 14
        HStack(alignment: .top, spacing: 0, content: {
            FeedCellView(item: entry.items.first!)
            Image("iconWidget")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(height: 25)
        })
        .padding(EdgeInsets.init(top: hPadding, leading: hPadding, bottom: hPadding, trailing: hPadding))
        .background(BackgroundGradient())
        .widgetURL(productDeepLink(productId: String(entry.items.first!.productId)))
    }
}

struct BackgroundGradient: View {
    let topColor = Color.init(rgb: 0xdf4159)
    let bottomColor = Color.init(rgb: 0xbc416b)
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [topColor, bottomColor]), startPoint: .top, endPoint: .bottom)
    }
}

struct MediumFeedView : View {
    var entry: WidgetTimelineProvider.Entry
    
    var body: some View {
        let hPadding:CGFloat = 10
        let insets = EdgeInsets.init(top: hPadding, leading: 20, bottom: hPadding, trailing: 0)
        VStack (alignment: .center, spacing: 0, content: {
            WidgetHeader(title: getFeedTitleText(category: entry.feedCategory))
            FeedCellsRow(items: Array(entry.items[0..<(min(4, entry.items.count))]))
                .padding(insets)
        })
        .background(BackgroundGradient())
    }
}

struct LargeFeedView : View {
    var entry: WidgetTimelineProvider.Entry
    
    var body: some View {
        let hPadding:CGFloat = 25
        let insets = EdgeInsets.init(top: 0, leading: hPadding, bottom: 0, trailing: hPadding)
        VStack (alignment: .center, spacing: 0, content: {
            WidgetHeader(title: getFeedTitleText(category: entry.feedCategory))
            Spacer(minLength: hPadding)
            let columns:UInt = 3
            FeedCellsRow(items: getNthXFrom(entry.items, nth: 0, x: columns))
                .padding(insets)
            Spacer(minLength: hPadding)
            FeedCellsRow(items: getNthXFrom(entry.items, nth: 1, x: columns))
                .padding(insets)
            Spacer(minLength: hPadding)
        })
        .background(BackgroundGradient())
    }
}

private func getNthXFrom<T>(_ source:Array<T>, nth:UInt, x:UInt) -> Array<T> {
    if (nth >= source.count) {
        return []
    }
    let startIndex = min(Int(nth * x), source.count)
    let endIndex:Int = min(startIndex + Int(x-1), source.count-1)
    return Array(source[startIndex...endIndex])
}

struct WidgetUI_Previews: PreviewProvider {
    
    static var previews: some View {
        Group {
            WidgetFeedView(entry: FeedTimelineEntry(date: Date(), items: generatePlaceholderProvider(6), feedCategory:.latest))
                .previewContext(WidgetPreviewContext(family: .systemLarge))
            WidgetFeedView(entry: FeedTimelineEntry(date: Date(), items: generatePlaceholderProvider(6), feedCategory:.latest))
                .previewContext(WidgetPreviewContext(family: .systemMedium))
            WidgetFeedView(entry: FeedTimelineEntry(date: Date(), items: generatePlaceholderProvider(1), feedCategory:.latest))
                .previewContext(WidgetPreviewContext(family: .systemSmall))
        }
    }
}

private func productDeepLink(productId:String) -> URL {
    return URL(string: "thegadgetflow://product_id=" + productId)!
}

private func getFeedTitleText(category:WidgetFeedCategory) -> String {
    switch (category) {
    case .latest: return "NEW DISCOVERIES"
    case .wishlist: return "WISH LIST"
    default: return ""
    }
}
