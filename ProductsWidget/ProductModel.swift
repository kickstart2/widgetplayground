//
//  ProductModel.swift
//  ProductsWidget
//
//  Created by Anton on 30.08.20.
//

import Foundation


struct ProductModel: Decodable, Equatable {
    var productId: Int
    var imageURL: String
    var title: String
    var description: String
    
    enum CodingKeys: String, CodingKey {
        case productId = "id"
        case imageURL = "image_url"
        case title
        case description = "content"
    }
    
//    init(from decoder:Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        productId = try String(container.decode(Int.self, forKey: CodingKeys.productId))
//
////        index = container.codingPath.first!.stringValue
//    }
}
