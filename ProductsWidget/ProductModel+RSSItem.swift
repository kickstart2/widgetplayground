//
//  ProductModel+RSSItem.swift
//  ProductsWidget
//
//  Created by Anton on 30.08.20.
//

import Foundation
import GadgetFlowComponents

extension ProductModel {
    init(item: RSSItem) {
        self.init(productId:Int(item.rss_id)!, imageURL:item.rss_image, title:item.rss_title, description:item.rss_description)
    }
}
