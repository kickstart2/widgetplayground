//
//  UIColor+RGB.swift
//  ProductsWidget
//
//  Created by Anton on 31.08.20.
//

import Foundation
import SwiftUI

extension Color {
    init(rgb: UInt) {
        self.init(red: Double((rgb & 0xFF0000) >> 16) / 255.0, green: Double((rgb & 0x00FF00) >> 8) / 255.0, blue: Double(rgb & 0x0000FF) / 255.0)
        }
}
