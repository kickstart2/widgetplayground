//
//  ImageViewContainer.swift
//  ProductsWidget
//
//  Created by Anton on 30.08.20.
//

import Foundation
import SwiftUI
import WidgetKit

struct ImageViewContainer: View {
    private let url: URL?
    
    init(imageURL: String) {
        url = URL(string: imageURL)
    }
    
    var body: some View {
        Group {
             if let url = url, let imageData = try? Data(contentsOf: url),
               let uiImage = UIImage(data: imageData) {

               Image(uiImage: uiImage)
                 .resizable()
                 .aspectRatio(contentMode: .fill)
              }
              else {
                Image("iconPlaceholder")
                    .aspectRatio(contentMode: .fill)
              }
            }
//        Text(String( self.remoteImageURL.count))
//        (self.remoteImageURL.data) ? Text("1") : Text("2")
//        Image(uiImage: (self.remoteImageURL.data.isEmpty) ? UIImage(imageLiteralResourceName: "iconPlaceholder") : UIImage(data: self.remoteImageURL.data)!)
//            .resizable()
//            .aspectRatio(contentMode: .fit)
//            .frame(width: 250, height: 250)
    }
}

struct ImageViewContainer_Previews: PreviewProvider {
    static var previews: some View {
        ImageViewContainer(imageURL: "https://thegadgetflow.com/wp-content/uploads/2020/08/Akai-Pro-MPK-mini-mk3-Small-Keyboard-Controller-011-1024x576.jpg?gfldp=true")
            .previewContext(WidgetPreviewContext(family: .systemSmall))
        
            
    }
}
