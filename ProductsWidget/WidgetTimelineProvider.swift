//
//  WidgetTimelineProvider.swift
//  ProductsWidget
//
//  Created by Anton on 27.08.20.
//

import Foundation
import WidgetKit
import GadgetFlowComponents

class SingleProductTimelineModel {
    private var currentProductId:Int!
    private var currentProductIndex:Int = 0
    
    public func pickProductFrom(_ items:[ProductModel]) -> ProductModel? {
        if (items.isEmpty) {
            return nil
        }
        
        if (currentProductId == nil ||
                currentProductIndex >= items.count - 1 ||
                items[currentProductIndex].productId != currentProductId) {
            currentProductIndex = 0
        } else {
            currentProductIndex = currentProductIndex + 1
        }
        
        let product:ProductModel = items[currentProductIndex]
        currentProductId = product.productId
        return product
    }
}

struct WidgetTimelineProvider: IntentTimelineProvider {
    public typealias Entry = FeedTimelineEntry
    
    private let snapshotModel = SnapshotViewModel()
    private let singleProductTimelineModel:SingleProductTimelineModel = SingleProductTimelineModel()
    
    public func placeholder(in context: Context) -> Entry {
        return Entry(date: Date(), items: generatePlaceholderProvider(6), feedCategory: .unknown)
    }

    public func getSnapshot(for configuration: FeedCategoryIntent, in context: Context, completion: @escaping (Entry) -> Void) {
        let entry = Entry(date: Date(), items: generateSnapshotProvider(6), feedCategory: configuration.category)
        completion(entry)
    }
    
    func generateSnapshotProvider(_ size: Int) -> [ProductModel] {
        return snapshotModel.getProducts(max: size, random: true)
    }
    
    public func getTimeline(for configuration: FeedCategoryIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        
        generateTimeline(for: configuration.category, in: context, completion: completion)
    }
    
    private func createFeedLoader(for category:WidgetFeedCategory) -> IFeedLoader {
        
        switch(category) {
        case .latest: return FeedLoader()
        case .wishlist: return WishListLoader()
        default: return FeedLoader()
        }
    }
    
    private func generateTimeline(for category:WidgetFeedCategory, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
        
        let contentLoader = createFeedLoader(for: category)
        contentLoader.load {items in
//            let items = mutableItems.compactMap { ProductModel(item: ($0 as! RSSItem)) }
            let (refreshDate, products) = (context.family == .systemSmall) ? singleProductEntry(items) : feedEntity(items)
            
            let entry = Entry(date: Date(), items: products, feedCategory: category)

            let timeline = Timeline(entries: [entry], policy: .after(refreshDate))
            completion(timeline)
        }
    }
    
    private func singleProductEntry(_ items:[ProductModel]) -> (Date, [ProductModel]) {
        let currentDate = Date()
        let refreshDate = Calendar.current.date(byAdding: .minute, value: 15, to: currentDate)!
        var products:[ProductModel] = []
        if let product = singleProductTimelineModel.pickProductFrom(items) {
            products.append(product)
        }
        return (refreshDate, products)
    }
    
    private func feedEntity(_ items:[ProductModel]) -> (Date, [ProductModel]) {
        let currentDate = Date()
        let refreshDate = Calendar.current.date(byAdding: .hour, value: 12, to: currentDate)!
        let items = items
        return (refreshDate, items)
    }
}

func generatePlaceholderProvider(_ size:Int) -> [ProductModel] {
    let previewItem = ProductModel(
        productId: 123,
        imageURL: "https://thegadgetflow.com/wp-content/uploads/2020/08/Akai-Pro-MPK-mini-mk3-Small-Keyboard-Controller-011-1024x576.jpg?gfldp=true",
        title: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,", description: "Description")
    var result:[ProductModel] = []
    for _ in 0..<size {
        result.append(previewItem)
    }
    return result
}

protocol IFeedLoader {
    func load(completion: @escaping ([ProductModel]) -> Void)
}

struct FeedLoader:IFeedLoader {
    let loader:GFFeedDataLoader
    
    init() {
        self.loader = GFFeedDataLoader()
        self.loader.feedType = STR_FEED_ARCHIVE
        self.loader.numberProductsPerRequest = 6
    }
    
    public func load(completion: @escaping ([ProductModel]) -> Void) {
        self.loader.didReceiveResults = {mutableItems in
            let items = mutableItems.compactMap { ProductModel(item: ($0 as! RSSItem)) }
            completion(items)
        }
        self.loader.fetchFirstPage(false)
    }
}

struct WishListLoader:IFeedLoader {
    public func load(completion: @escaping ([ProductModel]) -> Void) {
        GFWishlistManager.shared()?.provideWishlist({ (folders:[GFWishlistFolderDO]?) in
            if let folders = folders {
                let productItems = folders.reduce([], {$0 + $1.arrItems.map({ ProductModel(item: $0 as! RSSItem) }) })
                completion(productItems)
                return
            }
            completion([])
        }, refreshFromServer: true)
    }
}
