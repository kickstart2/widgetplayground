# iOS Widget Playground

Quick Sample of new Widget for iOS 14 (SwiftUI, WidgetKit)

- Feed data fetched from products list
- Intent is used as widget settings to configure type of content requested form server
- Binding is not possible inside AppExtension



Project Requires GadgetFlowComponents framework

Note: Xcode Version 12 is required 
(Also works with xcode 12.0 beta 6 including appstore delivery) 

TODO:
- preload images while constructing TimelineProvider
- prepare view items in different sizes for iOS widget configurator
  
