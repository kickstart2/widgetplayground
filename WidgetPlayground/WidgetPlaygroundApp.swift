//
//  WidgetPlaygroundApp.swift
//  WidgetPlayground
//
//  Created by Anton on 02.09.20.
//

import SwiftUI

@main
struct WidgetPlaygroundApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
