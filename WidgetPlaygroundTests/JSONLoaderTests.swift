//
//  JSONLoaderTests.swift
//  WidgetPlaygroundTests
//
//  Created by Anton on 27.09.20.
//

import XCTest

struct EmbedCodable: Decodable, Equatable {
    var value: String
}

class JSONLoaderTests: XCTestCase {
    
    struct EmptyCodable:Codable {}

    func testRead() throws {
        XCTAssertNotNil(try JSONLoader().readFromBundle(file: "foobar", as: EmptyCodable.self), "foobar.json loaded")
        
//        XCTAssertNotNil(try JSONLoader().readFromURL(URL(string: "http://sefsef")!, as: TestCodable.self), "json from url loaded")
    }
    
    func testDecode() throws {
        let json = "{\"foo\":\"bar\"}"
        XCTAssertNotNil(try JSONLoader().decode(DecodedArray<EmbedCodable>.self, from: json.data(using: .utf8)!), "decode simple json")
    }
    
    func testErrors() {
        let json = "{"
        XCTAssertThrowsError(try JSONLoader().decode(DecodedArray<EmbedCodable>.self, from: json.data(using: .utf8)!), "should throw .parseError") {error in
            XCTAssertEqual(error as? ParsingError, .parseError)
        }
        
        XCTAssertThrowsError(try JSONLoader().readFromBundle(file: "noFile", as: EmptyCodable.self), "should throw .fileNotFound") {error in
            XCTAssertEqual(error as? ParsingError, .fileNotFound)
        }
        
        XCTAssertThrowsError(try JSONLoader().readFromURL(URL(string: "someUrl")!, as: EmptyCodable.self), "should throw .loadError") {error in
            XCTAssertEqual(error as? ParsingError, .loadError)
        }
        
        XCTAssertThrowsError(try JSONLoader().readFromBundle(file: "foobar", as: [EmptyCodable].self), "should throw .parseError") {error in
            XCTAssertEqual(error as? ParsingError, .parseError)
        }
    }
    
    func testCodable() {
        XCTAssertNoThrow(try JSONLoader().readFromBundle(file: "foobar", as: DecodedArray<EmbedCodable>.self), "decode simple json")
        
        XCTAssertEqual(try JSONLoader().readFromBundle(file: "foobar", as: DecodedArray<EmbedCodable>.self).array.count, 3, "decoded all 3 EmbedCodable nodes")
    }

}
