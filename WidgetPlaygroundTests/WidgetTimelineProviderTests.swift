//
//  WidgetTimelineProviderTests.swift
//  WidgetPlaygroundTests
//
//  Created by Anton on 29.09.20.
//

import XCTest

class WidgetTimelineProviderTests: XCTestCase {
    
    var model:SingleProductTimelineModel!

    override func setUpWithError() throws {
        model = SingleProductTimelineModel()
    }

    override func tearDownWithError() throws {
        model = nil
    }
    
    func testItemsGenerator() throws {
        let items = generateItems(3, startWith: 0)
        XCTAssertEqual(items.count, 3, "generated 3 products")
        XCTAssertEqual(items.first?.productId, 0, "numeration started with 0")
        
        let itemsFrom100 = generateItems(3, startWith: 100)
        XCTAssertEqual(itemsFrom100.first?.productId, 100, "numeration started with 100")
        XCTAssertEqual(itemsFrom100.last?.productId, 102, "last item is is 102")
        
        let itemsEmpty = generateItems(0, startWith: 0)
        XCTAssertEqual(itemsEmpty.count, 0, "empty items")
    }
    
    func testSingleProductFromEmptyItems() throws {
        let items = generateItems(0, startWith: 0)
        XCTAssertEqual(items.count, 0, "empty items")
        let product = model.pickProductFrom(items)
        XCTAssertNil(product, "product from empty items does not exist")
    }

    func testSingleProductSelectorLoop() throws {
        let items = generateItems(3, startWith: 0)
        
        var product = model.pickProductFrom(items)
        XCTAssertEqual(product?.productId, items[0].productId, "very first product is first from items")
        product = model.pickProductFrom(items)
        XCTAssertEqual(product?.productId, items[1].productId, "second pick should return second product")
        product = model.pickProductFrom(items)
        product = model.pickProductFrom(items)
        XCTAssertEqual(product?.productId, items[0].productId, "fourth pick should return first product")
    }
    
    func testSingleProductSelector() throws {
        let items = generateItems(2, startWith: 0)
        
        var product = model.pickProductFrom(items)
        product = model.pickProductFrom(items)
        XCTAssertEqual(product?.productId, items[1].productId, "second pick should return second product")
        
        let newItems = generateItems(4, startWith: 10)
        product = model.pickProductFrom(newItems)
        XCTAssertEqual(product?.productId, newItems[0].productId, "pick from new items should return first new product")
        product = model.pickProductFrom(newItems)
        product = model.pickProductFrom(newItems)
        XCTAssertEqual(product?.productId, newItems[2].productId, "3rd product")
    }
    
    func generateItems(_ count:Int, startWith startIndex:Int) -> [ProductModel] {
        var result:[ProductModel] = []
        for index in 0..<count {
            let id = startIndex + index
            result.append(ProductModel(productId: id, imageURL: "", title: "title \(id)", description: "description \(id)"))
        }
        return result
    }

}
