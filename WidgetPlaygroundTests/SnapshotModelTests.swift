//
//  SnapshotModelTests.swift
//  WidgetPlaygroundTests
//
//  Created by Anton on 26.09.20.
//

import XCTest
@testable import GadgetFlowWidgets

class SnapshotModelTests: XCTestCase {

    var snapshotViewModel: SnapshotViewModel! = nil
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        snapshotViewModel = SnapshotViewModel()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        snapshotViewModel = nil
    }

    func testSnapshotItems() throws {
        let products = snapshotViewModel.getProducts()
        XCTAssertNotNil(products, "products not empty")
        
        XCTAssertEqual(products?.first?.productId, 438760, "first product has id 438760")
        
        let limitedProducts = snapshotViewModel.getProducts(max: 5, random: false)
        XCTAssertNotNil(limitedProducts, "limitedProducts not empty")
        
        XCTAssertEqual(limitedProducts.first?.productId, 438760, "first limitedProducts has id 438760")
        XCTAssertEqual(limitedProducts.count, 5, "products count limited by 5")
        
        XCTAssertEqual(limitedProducts, Array(products![0...4]), "products not shuffled")
        
    }
    
    func testRandomItems() throws {
        let preProducts = snapshotViewModel.getProducts()
        XCTAssertEqual(preProducts?.count, 10, "10 products before shuffle")
        
        let products1 = snapshotViewModel.getProducts(max:10, random: true)
        let products2 = snapshotViewModel.getProducts(max:10, random: true)
        
        XCTAssertNotEqual(products1, products2, "products are random")
        
        let products3 = snapshotViewModel.getProducts(max:1, random: true)
        let products4 = snapshotViewModel.getProducts(max:1, random: true)
        
        XCTAssertNotEqual(products3, products4, "products limited to 1 are random")
        
        let postProducts = snapshotViewModel.getProducts()
        XCTAssertEqual(postProducts, preProducts, "products are not damaged by shuffle")
    }

}
